{
  "$schema": "https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json",
  "basics": {
    "name": "Alexandre Monties",
    "label": "Software Craftsman",
    "picture": "https://media.licdn.com/dms/image/C4D03AQGLm2E283o5jA/profile-displayphoto-shrink_200_200/0/1606040622844?e=1678320000&v=beta&t=vxewtOsVbwqZFtuB72eeENK8xrClqmmXDzKIyT1Kfds",
    "email": "alexandre.monties@gmail.com",
    "phone": "+33685258215",
    "url": "http://ch3rub1.gitlab.io/resume/",
    "summary": "Passionate about innovation and technology, I enjoy experimenting, discovering new tools, and am constantly seeking new challenges.\nAn advocate of software craftsmanship and test-driven development, I cannot imagine creating a poorly designed and untested product.\nFinally, I enjoy discussing and exchanging ideas with my colleagues while enjoying a good cup of coffee ☕.",
    "location": {
      "postalCode": "13109",
      "city": "Simiane-Collongue",
      "countryCode": "FR",
      "region": "Provence-Alpes-Côte d'Azur"
    },
    "profiles": [
      {
        "network": "LinkedIn",
        "username": "alexandremonties",
        "url": "https://www.linkedin.com/in/alexandremonties/"
      },
      {
        "network": "GitHub",
        "username": "ch3rub1",
        "url": "https://github.com/ch3rub1/"
      },
      {
        "network": "GitLab",
        "username": "ch3rub1",
        "url": "https://gitlab.com/ch3rub1/"
      }
    ]
  },
  "work": [
    {
      "name": "Agicap",
      "location": "Lyon",
      "description": "Cashflow management software editor",
      "position": "Senior Software Developer",
      "url": "https://agicap.com/",
      "startDate": "2022-01-10",
      "summary": "I joined Agicap in order to discover a new professional environment, that of startups, and a new technical stack that I had only very rarely had the opportunity to experiment with.\nI joined the team responsible for the evolution and maintenance of the company's flagship product and actively work every day to address the many challenges related to a product and a company that is constantly evolving.",
      "highlights": [
        "Develop new feature for the company core product line",
        "Refactor legacy implementations of main features",
        "Build an asynchronous, IA based µservice in Python",
        "Bootstrap a level 3 REST API initiative",
        "Performance audit"
      ]
    },
    {
      "name": "Softway Medical",
      "location": "Meyreuil",
      "description": "Health software editor",
      "position": "Software Architect",
      "url": "https://www.softwaymedical.fr/",
      "startDate": "2016-10-01",
      "endDate": "2021-12-11",
      "summary": "After a year as an external consultant, I joined Softway Medical as a software architect.\nI worked in collaboration with many departments to develop new practices and methodologies such as test-driven development, software craftsmanship, continuous integration, continuous delivery, and design system.\nIn addition, I actively participated in a project to begin transitioning from a monolithic architecture to a distributed one.",
      "highlights": [
        "Technical guidance and support to 20+ functional squads.",
        "Migration from a monolithic architecture to a distributed one, based on µservices.",
        "Applications’ delivery processes (CI/CD) redesigning.",
        "Build an application to help federal prison pharmacies to organize drugs prescriptions for inmates.",
        "Build a framework agnostic UI components library, in collaboration with UX team, in order to implement a design system."
      ]
    },
    {
      "name": "Apside",
      "location": "Aix-en-Provence",
      "description": "Consulting firm",
      "position": "Software Engineer",
      "url": "https://apside.com/",
      "startDate": "2015-11-01",
      "endDate": "2016-09-01",
      "summary": "As an external consultant for Apside, I joined Softway Medical to help migrate a set of Java applets to a more robust solution.\nThese applets were designed to allow for greater interactivity between the company's main web application and users' computers.\nThis project was both critical and exciting as these applets were essential to the daily tasks of many users.",
      "highlights": [
        "Build a lightweight modular client software to enable interactions between a web application and user computer's resources.",
        "Technical support to customers and users."
      ]
    },
    {
      "name": "Atos",
      "location": "Aix-en-Provence",
      "description": "Consulting firm",
      "position": "Software Engineer",
      "url": "https://atos.net/",
      "startDate": "2013-10-01",
      "endDate": "2015-10-01",
      "summary": "I began my professional career as an external consultant for the French Military Navy to redesign the entire supply chain and maintenance of the fleet.\nDuring this professional experience, I regularly interacted with high-ranking military personnel to ensure the production of tools that met their requirements.",
      "highlights": [
        "Build an information system to organize military ships maintenance and repairs at the dock.",
        "Build a software allowing military ships to request replacement parts once at sea.",
        "User training."
      ]
    }
  ],
  "education": [
    {
      "institution": "Luminy's Institute of Science",
      "url": "https://sciences.univ-amu.fr/",
      "area": "Information Technology",
      "studyType": "Master",
      "startDate": "2011-09-01",
      "endDate": "2013-06-30",
      "gpa": "Magna Cum Laude"
    },
    {
      "institution": "Luminy's Institute of Science",
      "url": "https://sciences.univ-amu.fr/",
      "area": "Information Technology",
      "studyType": "Bachelor",
      "startDate": "2010-09-01",
      "endDate": "2011-06-30",
      "gpa": "Cum Laude"
    },
    {
      "institution": "Aix-en-Provence's Institute of Technology",
      "url": "https://iut.univ-amu.fr/",
      "area": "Information Technology",
      "studyType": "Associate",
      "startDate": "2007-09-01",
      "endDate": "2010-06-30"
    },
    {
      "institution": "Joliot-Curie High School",
      "url": "http://www.lyc-joliotcurie.ac-aix-marseille.fr/",
      "area": "Engineering Sciences",
      "studyType": "High School",
      "startDate": "2004-09-01",
      "endDate": "2007-06-30"
    }
  ],
  "awards": [
    {
      "title": "Valedictorian",
      "date": "2013-06-30",
      "awarder": "Luminy's Institute of Science"
    }
  ],
  "skills": [
    {
      "name": "Java Ecosystem",
      "level": "Advanced",
      "keywords": [
        "Java",
        "Kotlin",
        "Spring",
        "Quarkus",
        "OSGi"
      ]
    },
    {
      "name": "Javascript Ecosystem",
      "level": "Advanced",
      "keywords": [
        "Javascript",
        "Typescript",
        "Angular",
        "Vue.js",
        "Stencil.js"
      ]
    },
    {
      "name": ".NET Ecosystem",
      "level": "Intermediate",
      "keywords": [
        "C#",
        ".NET Framework",
        "Entity Framework",
        "MediatR"
      ]
    },
    {
      "name": "Python Ecosystem",
      "level": "Beginner",
      "keywords": [
        "Python"
      ]
    },
    {
      "name": "Rust Ecosystem",
      "level": "Beginner",
      "keywords": [
        "Rust",
        "Rocket"
      ]
    },
    {
      "name": "Containers Ecosystem",
      "level": "Advanced",
      "keywords": [
        "Docker",
        "Kubernetes",
        "Openshift"
      ]
    },
    {
      "name": "CI/CD",
      "level": "Advanced",
      "keywords": [
        "GitLab CI",
        "Jenkins"
      ]
    },
    {
      "name": "SQL Databases",
      "level": "Advanced",
      "keywords": [
        "Oracle",
        "Postgre SQL",
        "SQL Server"
      ]
    },
    {
      "name": "NoSQL Databases",
      "level": "Intermediate",
      "keywords": [
        "Mongo"
      ]
    }
  ],
  "languages": [
    {
      "language": "French",
      "fluency": "Native speaker"
    },
    {
      "language": "English",
      "fluency": "Advanced"
    }
  ],
  "interests": [
    {
      "name": "Video Games",
      "keywords": [
        "Strategy",
        "Roguelite",
        "Souls Like",
        "Puzzle"
      ]
    },
    {
      "name": "Board Games",
      "keywords": [
        "Strategy",
        "Role Play",
        "Puzzle"
      ]
    },
    {
      "name": "Computer Hardware",
      "keywords": [
        "Building",
        "Overclocking"
      ]
    },
    {
      "name": "Metal Music",
      "keywords": [
        "Heavy",
        "Industrial",
        "Trash"
      ]
    }
  ],
  "meta": {
    "canonical": "https://raw.githubusercontent.com/jsonresume/resume-schema/master/resume.json",
    "version": "v1.0.0"
  }
}